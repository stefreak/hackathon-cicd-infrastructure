variable "external_network" {
  type    = "string"
  default = "caf8de33-1059-4473-a2c1-2a62d12294fa"
}

resource "openstack_networking_network_v2" "packer_net" {
  name           = "packer_net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "packer_subnet" {
  name            = "packer_subnet"
  network_id      = "${openstack_networking_network_v2.packer_net.id}"
  cidr            = "192.168.2.0/24"
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "1.0.0.1"]
}

resource "openstack_networking_router_v2" "packer_router" {
  name                = "packer_router"
  admin_state_up      = true
  external_network_id = "${var.external_network}"
}

resource "openstack_networking_router_interface_v2" "packer_routerint" {
  router_id = "${openstack_networking_router_v2.packer_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.packer_subnet.id}"
}

resource "openstack_compute_secgroup_v2" "packer_allow_ssh" {
  name        = "packer_allow_ssh"
  description = "Allow inboud SSH"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

output "network_id" {
  value = "${openstack_networking_network_v2.packer_net.id}"
}
