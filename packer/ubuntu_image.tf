resource "openstack_images_image_v2" "ubuntu_bionic" {
  name             = "Ubuntu 18.04"
  image_source_url = "https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
  container_format = "bare"
  disk_format      = "qcow2"
}

output "image_ubuntu_bionic_name" {
  value = "${openstack_images_image_v2.ubuntu_bionic.name}"
}
