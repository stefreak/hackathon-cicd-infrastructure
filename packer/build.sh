#!/bin/bash

set -e

image_type="${1}"

# Create resources needed for packer, like a network and security group
#trap "{ terraform destroy -auto-approve; }" EXIT
terraform apply -auto-approve

# Add terraform output to environment variables
terraform output | awk -F' = ' '{print "export "$1"=\""$2"\""}' > output.env
source output.env
rm output.env

# Build the packer image
export out_image_name="${image_type} Ubuntu 18.04"
packer build ${image_type}.json
