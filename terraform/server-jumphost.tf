resource "openstack_compute_instance_v2" "jumphost" {
  name            = "hackathon-jumphost"
  image_id        = "${data.openstack_images_image_v2.ubuntu-bionic.id}"
  flavor_name     = "m1.micro"
  key_pair        = "${openstack_compute_keypair_v2.hackathon_sshkey.name}"
  security_groups = [
    "default",
    "${openstack_compute_secgroup_v2.sg_ssh.name}"
  ]

  network {
    name = "${openstack_networking_network_v2.net_hackathon.name}"
  }
}

resource "openstack_compute_floatingip_v2" "fip_jumphost" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "fipas_jumphost" {
  floating_ip = "${openstack_compute_floatingip_v2.fip_jumphost.address}"
  instance_id = "${openstack_compute_instance_v2.jumphost.id}"
}

output "ssh_jumphost" {
  value = "ssh -A -l ubuntu ${openstack_compute_floatingip_v2.fip_jumphost.address}"
}