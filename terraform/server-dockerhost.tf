resource "openstack_compute_instance_v2" "dockerhost" {
  count           = 1
  name            = "docker-host-${count.index}"
  image_id        = "${data.openstack_images_image_v2.dockerhost-ubuntu-bionic.id}"
  flavor_name     = "m1.micro"
  key_pair        = "${openstack_compute_keypair_v2.hackathon_sshkey.name}"
  security_groups = [
    "default",
    "${openstack_compute_secgroup_v2.sg_ssh.name}",
    "${openstack_compute_secgroup_v2.sg_web.name}"
  ]

  network {
    name = "${openstack_networking_network_v2.net_hackathon.name}"
  }

  # See also https://www.hashicorp.com/blog/zero-downtime-updates-with-terraform
  lifecycle {
    create_before_destroy = true
  }

  provisioner "local-exec" {
    # Wait until service is available.
    # More optimal solution would be to conduct
    # an actual healthcheck but ¯\_(ツ)_/¯
    command = "sleep 60"
  }
}

resource "openstack_lb_member_v2" "lb_app_pool_members_production" {
  count         = "${openstack_compute_instance_v2.dockerhost.count}"
  address       = "${element(openstack_compute_instance_v2.dockerhost.*.access_ip_v4, count.index)}"
  protocol_port = 80
  pool_id       = "${openstack_lb_pool_v2.lb_app_production_pool.id}"
  name          = "${element(openstack_compute_instance_v2.dockerhost.*.name, count.index)}"
  subnet_id     = "${openstack_networking_subnet_v2.subnet_hackathon.id}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "openstack_lb_member_v2" "lb_app_pool_members_staging" {
  count         = "${openstack_compute_instance_v2.dockerhost.count}"
  address       = "${element(openstack_compute_instance_v2.dockerhost.*.access_ip_v4, count.index)}"
  protocol_port = 8080
  pool_id       = "${openstack_lb_pool_v2.lb_app_staging_pool.id}"
  name          = "${element(openstack_compute_instance_v2.dockerhost.*.name, count.index)}"
  subnet_id     = "${openstack_networking_subnet_v2.subnet_hackathon.id}"

  lifecycle {
    create_before_destroy = true
  }
}
