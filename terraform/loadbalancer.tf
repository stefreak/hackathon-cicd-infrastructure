
resource "openstack_lb_loadbalancer_v2" "lb_app" {
  vip_subnet_id = "${openstack_networking_subnet_v2.subnet_hackathon.id}"
  name          = "application loadbalancer"
  security_group_ids = [
    "${openstack_compute_secgroup_v2.sg_web.id}"
  ]
}

resource "openstack_lb_listener_v2" "lb_app_production_listener" {
  protocol        = "TCP"
  protocol_port   = 80
  loadbalancer_id = "${openstack_lb_loadbalancer_v2.lb_app.id}"
}

resource "openstack_lb_listener_v2" "lb_app_staging_listener" {
  protocol        = "TCP"
  protocol_port   = 8080
  loadbalancer_id = "${openstack_lb_loadbalancer_v2.lb_app.id}"
}

resource "openstack_lb_pool_v2" "lb_app_production_pool" {
  protocol    = "TCP"
  lb_method   = "ROUND_ROBIN"
  listener_id = "${openstack_lb_listener_v2.lb_app_production_listener.id}"
}

resource "openstack_lb_pool_v2" "lb_app_staging_pool" {
  protocol    = "TCP"
  lb_method   = "ROUND_ROBIN"
  listener_id = "${openstack_lb_listener_v2.lb_app_staging_listener.id}"
}

# XXX: Due to a bug I cannot have multiple healthmonitors per loadbalancer.
# This should be investigated.
#resource "openstack_lb_monitor_v2" "monitor_staging" {
#  pool_id     = "${openstack_lb_pool_v2.lb_app_staging_pool.id}"
#  type        = "TCP"
#  delay       = 5
#  timeout     = 10
#  max_retries = 2
#}

resource "openstack_lb_monitor_v2" "monitor_production" {
  pool_id     = "${openstack_lb_pool_v2.lb_app_production_pool.id}"
  type        = "TCP"
  delay       = 5
  timeout     = 10
  max_retries = 2
}

resource "openstack_networking_floatingip_v2" "fip_hackathon_lb" {
  pool = "ext-net"
}

resource "openstack_networking_floatingip_associate_v2" "fipas_hackathon_lb" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_hackathon_lb.address}"
  port_id = "${openstack_lb_loadbalancer_v2.lb_app.vip_port_id}"
}

output "http_production" {
  value = "http://${openstack_networking_floatingip_v2.fip_hackathon_lb.address}"
}

output "http_staging" {
  value = "http://${openstack_networking_floatingip_v2.fip_hackathon_lb.address}:8080"
}
