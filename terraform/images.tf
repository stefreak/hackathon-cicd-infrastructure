data "openstack_images_image_v2" "dockerhost-ubuntu-bionic" {
  name        = "dockerhost Ubuntu 18.04"
  most_recent = true
}

data "openstack_images_image_v2" "ubuntu-bionic" {
  name        = "base Ubuntu 18.04"
  most_recent = true
}
