resource "openstack_compute_keypair_v2" "hackathon_sshkey" {
  name       = "hackathon_sshkey"
  public_key = "${var.ssh-public-key}"
}
