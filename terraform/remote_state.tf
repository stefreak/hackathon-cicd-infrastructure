terraform {
  backend "s3" {
    # For information how to create the S3 bucket, see
    # https://docs.syseleven.de/syseleven-stack/en/documentation/object-storage
    bucket                      = "steffensterraform"
    key                         = "terraform.tfstate"
    endpoint                    = "s3.cbk.cloud.syseleven.net"
    region                      = "cbk"
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

