variable "ssh-public-key" {
  type        = "string"
  description = "Public key for SSH"
}
