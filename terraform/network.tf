data "openstack_networking_network_v2" "ext-net" {
  name = "ext-net"
}

resource "openstack_networking_network_v2" "net_hackathon" {
  name           = "net_hackathon"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_hackathon" {
  name       = "subnet_hackathon"
  network_id = "${openstack_networking_network_v2.net_hackathon.id}"
  cidr       = "192.168.2.0/24"
  ip_version = 4
  dns_nameservers = ["1.1.1.1", "1.0.0.1"]
}

resource "openstack_networking_router_v2" "router_hackathon" {
  name                = "router_hackathon"
  admin_state_up      = true
  external_network_id = "${data.openstack_networking_network_v2.ext-net.id}"
}

resource "openstack_networking_router_interface_v2" "routerint_hackathon" {
  router_id = "${openstack_networking_router_v2.router_hackathon.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_hackathon.id}"
}

resource "openstack_compute_secgroup_v2" "sg_ssh" {
  name        = "hackathon_sg_ssh"
  description = "Allow inboud SSH"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_compute_secgroup_v2" "sg_web" {
  name        = "hackathon_sg_web"
  description = "Allow inboud HTTP"

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 8080
    to_port     = 8080
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}