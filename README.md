# hackathon-cicd-infrastructure

## Architecture of resulting system

```
                            _________________________________
                (internet) | (private network)               |
                           |                                 |
Internet <=> Loadbalancer <=> <dockerhost(s) with demo App>  |
                           |                                 |
              <jumphost for ssh access>                      |
                           |_________________________________|
```

## Packer

Packer is responsible to provide you with a working operating system layer, so that Terraform can start virtual machines that actually do something.

In this example, we also put the application inside the packer image, and instead of updating existing servers over time with new version of the app, we create new operating system images, create the new server with the new image, and delete the old server (this happens in terraform).

This approach is called immutable infrastructure.

## Terraform

In the terraform folder you find all the infrastructure that we need for our application, defined as code. Terraform will look at the current state in OpenStack, and compare that with the terraform resources in the code. If there are differences, it will take action and create / update / delete things in OpenStack, in an order that (most of the time) makes sense.

Terraform also stores some state, usually in *.tfstate files. In `remote_state.tf` we configured terraform to use the SysEleven-hosted version of the cloud object storage `S3`  instead to store the state.

Example terraform command
```
$ cd terraform
$ terraform output

http_production = http://185.56.128.79
http_staging = http://185.56.128.79:8080
ssh_jumphost = ssh -A -l ubuntu 185.56.128.200
```

For making sure that when replacing servers the user does not notice any downtime, we are using the approach described here: https://www.hashicorp.com/blog/zero-downtime-updates-with-terraform

## The application

Currently there is only one app running on this set up: https://gitlab.com/stefreak/hackathon-cicd-demo

It triggers the CI/CD pipeline of this repository using the GitLab pipeline trigger API.

## GitLab CI

GitLab is bringing everything here together. Have a look at `.gitlab-ci.yml`.

Cool thing that was possible because of automating everything here on GitLab: You can create daily / weekly schedules in GitLab CI that will build everything from scratch again, which in turns makes sure you always have the latest security updates installed. No manual work necessary anymore :)

All the secrets that are necessary for running the tools like terraform and packer are configured in the CI/CD settings in GitLab.

Namely these env variables are all the `OS_*` variables for OpenStack, and `AWS_SECRET_KEY` + `AWS_ACCESS_KEY` for S3

## Things you could do

- Run packer manually, write your own packer file
- Run terraform manually, write your own terraform code
- Replicate the CI/CD pipleline in your own GitLab account
- Come up with different / faster ways for deployment
- Build your own gitlab runner, for better security
- Come up with a way to guarantee that only one CI job runs terraform at a time. Currently there can be nasty race-conditions.
- Come up with a strategy how to deal with stateful applications like databases. Currently data would be deleted on every deployment :)
- Currently production and staging are not separated at all. The staging demo-app and production demo-app run on the same server. You could separate them more :)
- Old packer images will never removed right now, even when they are not used anymore. You could add a stage that cleans them up :)
